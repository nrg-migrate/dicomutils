/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class CStoreException extends DicomServiceException {
  private final static long serialVersionUID = 1L;
  
  public CStoreException() {}

  public CStoreException(final String message) {
    super(message);
  }

  public CStoreException(final Throwable cause) {
    super(cause);
  }

  public CStoreException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
