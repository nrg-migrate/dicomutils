/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class DicomContentException extends Exception {
  private final static long serialVersionUID = 1L;
  
  /**
   * 
   */
  public DicomContentException() { super(); }

  /**
   * @param message
   */
  public DicomContentException(final String message) {
    super(message);
  }

  /**
   * @param cause
   */
  public DicomContentException(final Throwable cause) {
    super(cause);
  }

  /**
   * @param message
   * @param cause
   */
  public DicomContentException(final String message, final Throwable cause) {
    super(message, cause);
   }
}
