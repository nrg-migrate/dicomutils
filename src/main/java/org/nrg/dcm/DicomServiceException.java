/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class DicomServiceException extends Exception {
  private final static long serialVersionUID = 1L;
  
  public DicomServiceException() {}

  public DicomServiceException(final String message) {
    super(message);
  }

  public DicomServiceException(final Throwable cause) {
    super(cause);
  }

  public DicomServiceException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
