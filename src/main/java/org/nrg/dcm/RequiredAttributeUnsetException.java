/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class RequiredAttributeUnsetException extends DicomContentException {
  private final static long serialVersionUID = 1L;
  
  public RequiredAttributeUnsetException(final DicomObject o, final int tag) {
    super("Required DICOM attribute " + o.nameOf(tag) + " is unset or empty");
  }
}
