/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.dcm;

import static org.nrg.dcm.TestFiles.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomOutputStream;

import junit.framework.TestCase;

import com.google.common.io.Files;
import com.google.common.io.InputSupplier;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DicomDirTest extends TestCase {
    private File sample, sample_headerless;

    public void testAddFile() throws IOException {
        final File ddf = File.createTempFile("DICOM", "DIR");
        ddf.deleteOnExit();
        final DicomDir dd = new DicomDir(ddf);
        dd.create();
        dd.addFile(sample);
        dd.close();
        ddf.delete();
    }

    public void testAddHeaderlessFile() throws IOException {
        final File ddf = File.createTempFile("DICOM", "DIR");
        ddf.deleteOnExit();
        final DicomDir dd = new DicomDir(ddf);
        dd.create();
        dd.addFile(sample_headerless);
        dd.close();
        ddf.delete();
    } 

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws IOException {
        sample = File.createTempFile("sample", ".dcm");
        sample.deleteOnExit();
        if (null == sampleDicomFile || !sampleDicomFile.isFile()) {
            final GZIPInputStream gzin = new GZIPInputStream(new FileInputStream(sampleDicomFileGzip));
            try {
                Files.copy(new InputSupplier<InputStream>() {
                    public InputStream getInput() {
                        return gzin;
                    }
                }, sample);
            } finally {
                gzin.close();
            }
        } else {
            Files.copy(sampleDicomFile, sample);
        }
        
        final DicomObject o = DicomUtils.read(sample);
        sample_headerless = File.createTempFile("sample_headerless", ".dcm");
        sample_headerless.deleteOnExit();
        final DicomOutputStream dos = new DicomOutputStream(sample_headerless);
        try {
            dos.writeDataset(o, o.getString(Tag.TransferSyntaxUID));
        } finally {
            dos.close();
        }
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() {
        sample.delete();
        sample_headerless.delete();
    }
}
