/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;

import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import com.google.common.io.OutputSupplier;

import junit.framework.TestCase;

import static org.nrg.dcm.TestFiles.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DicomUtilsTest extends TestCase {
    private File sample, samplegz;
    private DicomObject o;

    public void setUp() throws IOException {
        sample = File.createTempFile("sample", ".dcm");
        sample.deleteOnExit();
        if (null == sampleDicomFile || !sampleDicomFile.isFile()) {
            final GZIPInputStream gzin = new GZIPInputStream(new FileInputStream(sampleDicomFileGzip));
            try {
                Files.copy(new InputSupplier<InputStream>() {
                    public InputStream getInput() {
                        return gzin;
                    }
                }, sample);
            } finally {
                gzin.close();
            }
        } else {
            Files.copy(sampleDicomFile, sample);
        }
        
        samplegz = File.createTempFile("sample", ".dcm.gz");
        samplegz.deleteOnExit();
        if (null == sampleDicomFileGzip || !sampleDicomFileGzip.isFile()) {
            final GZIPOutputStream gzout = new GZIPOutputStream(new FileOutputStream(samplegz));
            try {
                Files.copy(sampleDicomFile, new OutputSupplier<OutputStream>() {
                    public OutputStream getOutput() {
                        return gzout;
                    }
                });
            } finally {
                gzout.close();
            }
        } else {
            Files.copy(sampleDicomFileGzip, samplegz);
        }
        
        o = DicomUtils.read(sample);
    }

    public void tearDown() {
        if (null != sample) {
            sample.delete();
        }
        if (null != samplegz) {
            samplegz.delete();
        }
    }

    /**
     * Test method for {@link org.nrg.dcm.DicomUtils#getStringRequired(org.dcm4che2.data.DicomObject, int)}.
     */
    public void testGetStringRequired() throws RequiredAttributeUnsetException {
        assertEquals("SIEMENS", DicomUtils.getStringRequired(o, Tag.Manufacturer));
        try {
            DicomUtils.getStringRequired(o, 0x00080071);
            fail("missed expected RequiredAttributeUnsetException");
        } catch (RequiredAttributeUnsetException ok) {}
    }
    
    public void testReadFile() throws IOException {
        final DicomObject o = DicomUtils.read(sample);
        assertEquals(UID.MRImageStorage, o.getString(Tag.SOPClassUID));
        assertEquals("Hospital", o.getString(Tag.InstitutionName));
        
        final DicomObject ogz = DicomUtils.read(samplegz);
        assertEquals(o, ogz);
        
        try {
            DicomUtils.read(new File("/no/such/file"));
            fail("expected IOException reading from nonexistent File");
        } catch (IOException ok) {}
    }
    
    public void testReadFileInteger() throws IOException {
        final DicomObject opart = DicomUtils.read(sample, Tag.Manufacturer);
        assertEquals("SIEMENS", opart.getString(Tag.Manufacturer));
        assertNull(opart.getString(Tag.InstitutionName));        
    }
    
    public void testReadFileURI() throws IOException {
        final URI uri = sample.toURI();
        final DicomObject o = DicomUtils.read(uri);
        assertEquals(UID.MRImageStorage, o.getString(Tag.SOPClassUID));
    }
    
    /**
     * Test method for {@link org.nrg.dcm.DicomUtils#stripTrailingChars(java.lang.StringBuilder, char)}.
     */
    public void testStripTrailingChars() {
        assertEquals("x", DicomUtils.stripTrailingChars(new StringBuilder("xyy"), 'y').toString());
        assertEquals("", DicomUtils.stripTrailingChars(new StringBuilder("yy"), 'y').toString());
        assertEquals("yx", DicomUtils.stripTrailingChars(new StringBuilder("yxyyy"), 'y').toString());
        assertEquals("yx", DicomUtils.stripTrailingChars(new StringBuilder("yx"), 'y').toString());

        assertEquals("x", DicomUtils.stripTrailingChars("xyy", 'y'));
    }
}
