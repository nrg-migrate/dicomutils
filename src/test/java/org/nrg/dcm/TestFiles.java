/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.dcm;

import java.io.File;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class TestFiles {
    public final static String SAMPLE_DICOM_PATH = System.getProperty("sample.data.dir");
    public final static String SAMPLE_DATA_FILENAME = "1.MR.head_DHead.4.23.20061214.091206.156000.8215318065.dcm";
    public final static File sampleDicomFile, sampleDicomFileGzip;
    static {
        final File sampleDir = new File(SAMPLE_DICOM_PATH);
        if (null != SAMPLE_DICOM_PATH && sampleDir.isDirectory()) {
            final File f = new File(sampleDir, SAMPLE_DATA_FILENAME);
            sampleDicomFile = f.isFile() ? f : null;
            final File fgz = new File(sampleDir, SAMPLE_DATA_FILENAME + ".gz");
            sampleDicomFileGzip = fgz.isFile() ? fgz : null;
        } else {
            sampleDicomFile = sampleDicomFileGzip = null;
        }
        if (null == sampleDicomFile && null == sampleDicomFileGzip) {
            // TODO: retrieve sample data
            throw new RuntimeException("Unable to find sample data: set system property sample.data.dir");
        }
    }
}
