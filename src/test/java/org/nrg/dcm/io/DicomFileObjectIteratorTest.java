/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.dcm.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import com.google.common.io.OutputSupplier;

import junit.framework.TestCase;

import static org.nrg.dcm.TestFiles.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DicomFileObjectIteratorTest extends TestCase {
    private File sample, samplegz;

    public void setUp() throws IOException {
        sample = File.createTempFile("sample", ".dcm");
        sample.deleteOnExit();
        if (null == sampleDicomFile || !sampleDicomFile.isFile()) {
            final GZIPInputStream gzin = new GZIPInputStream(new FileInputStream(sampleDicomFileGzip));
            try {
                Files.copy(new InputSupplier<InputStream>() {
                    public InputStream getInput() {
                        return gzin;
                    }
                }, sample);
            } finally {
                gzin.close();
            }
        } else {
            Files.copy(sampleDicomFile, sample);
        }

        samplegz = File.createTempFile("sample", ".dcm.gz");
        samplegz.deleteOnExit();
        if (null == sampleDicomFileGzip || !sampleDicomFileGzip.isFile()) {
            final GZIPOutputStream gzout = new GZIPOutputStream(new FileOutputStream(samplegz));
            try {
                Files.copy(sampleDicomFile, new OutputSupplier<OutputStream>() {
                    public OutputStream getOutput() {
                        return gzout;
                    }
                });
            } finally {
                gzout.close();
            }
        } else {
            Files.copy(sampleDicomFileGzip, samplegz);
        }
    }

    public void tearDown() {
        if (null != sample) {
            sample.delete();
        }
        if (null != samplegz) {
            samplegz.delete();
        }
    }

    public void testIterator() {
        final Set<File> dicoms = Sets.newHashSet(sample, samplegz);
        final Iterable<File> files = Arrays.asList(sample.getParentFile(), sample, new File("/etc/motd"), samplegz);
        final Iterator<Map.Entry<File,DicomObject>> iterator = new DicomFileObjectIterator(files);
        assertTrue(iterator.hasNext());
        final Map.Entry<File,DicomObject> e1 = iterator.next();
        assertTrue(dicoms.contains(e1.getKey()));
        dicoms.remove(e1.getKey());
        assertFalse(dicoms.contains(e1.getKey()));
        final DicomObject o1 = e1.getValue();
        final byte[] pixels1 = o1.getBytes(Tag.PixelData);
        assertNotNull(pixels1);
        assertTrue(iterator.hasNext());
        final Map.Entry<File,DicomObject> e2 = iterator.next();
        assertTrue(dicoms.contains(e2.getKey()));
        final DicomObject o2 = e2.getValue();
        final byte[] pixels2 = o2.getBytes(Tag.PixelData);
        assertFalse(e1.getKey().equals(e2.getKey()));
        assertTrue(Arrays.equals(pixels1, pixels2));        
        assertFalse(iterator.hasNext());
        try {
            iterator.next();
            fail("returned normally from next() on empty iterator");
        } catch (NoSuchElementException expected) {}
    }

    public void testIteratorWithStopTag() {
        final Set<File> dicoms = Sets.newHashSet(sample, samplegz);
        final Iterable<File> files = Arrays.asList(sample.getParentFile(), sample, new File("/etc/motd"));
        final Iterator<Map.Entry<File,DicomObject>> iterator = new DicomFileObjectIterator(files).setStopTag(0x00080032);
        assertTrue(iterator.hasNext());
        final Map.Entry<File,DicomObject> e1 = iterator.next();
        assertTrue(dicoms.contains(e1.getKey()));
        dicoms.remove(e1.getKey());
        assertFalse(dicoms.contains(e1.getKey()));
        final DicomObject o1 = e1.getValue();
        assertTrue(o1.contains(0x00080031));
        assertFalse(o1.contains(0x00080032));
        assertFalse(o1.contains(0x00080033));
        assertFalse(iterator.hasNext());
    }
    
    public void testDeleteFromIterator() {
        final Set<File> dicoms = Sets.newHashSet(sample, samplegz);
        final Iterable<File> files = Arrays.asList(sample.getParentFile(), sample, new File("/etc/motd"), samplegz);
        final Iterator<Map.Entry<File,DicomObject>> iterator = new DicomFileObjectIterator(files).setStopTag(Tag.PixelData);
        assertTrue(iterator.hasNext());
        try {
            iterator.remove();
            fail("returned without error from remove() on unstarted iterator");
        } catch (IllegalStateException expected) {}
        final Map.Entry<File,DicomObject> e1 = iterator.next();
        assertTrue(dicoms.contains(e1.getKey()));
        assertTrue(e1.getKey().exists());
        iterator.remove();
        assertFalse(e1.getKey().exists());
        dicoms.remove(e1.getKey());
        assertFalse(dicoms.contains(e1.getKey()));
        assertTrue(iterator.hasNext());
        final Map.Entry<File,DicomObject> e2 = iterator.next();
        assertTrue(dicoms.contains(e2.getKey()));
        assertTrue(e2.getKey().exists());
        assertFalse(e1.getKey().equals(e2.getKey()));
        assertFalse(iterator.hasNext());
        try {
            iterator.next();
            fail("returned normally from next() on empty iterator");
        } catch (NoSuchElementException expected) {}
    }


}
